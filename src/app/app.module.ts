import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { TestItemComponent } from './test/test-item/test-item.component';
import { ModalComponent } from './UI/modal/modal.component';
import { TestService } from './shared/test.service';
import { FormsModule } from '@angular/forms';
import { HelpComponent } from './help/help.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    ToolbarComponent,
    TestItemComponent,
    ModalComponent,
    HelpComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [TestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
