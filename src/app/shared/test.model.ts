export class Test {
  constructor(
    public question: string,
    public answer: string,
    public prompt: string,
    public status: boolean,
  ){}

  getAnswer(){
    return this.answer
  }
}
